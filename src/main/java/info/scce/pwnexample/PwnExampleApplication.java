package info.scce.pwnexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PwnExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PwnExampleApplication.class, args);
	}

}
