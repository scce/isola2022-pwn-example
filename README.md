# 5) Build Process

![Workflow](./workflow.png)*CI/CD Workflow Model*

## Requirements

1) The application is written in Java 11, thus the build process requires the Docker image `maven:3.8.5-jdk-11`.
2) In order to save disk space, we just deliver on the main branch.
3) ...
